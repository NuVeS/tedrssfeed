//
//  ViewController.m
//  TedRssFeed
//
//  Created by Максуд on 08.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import "ViewController.h"
#import "XMLParser.h"
#import "TableBuilder.h"

@interface ViewController ()

@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) TableBuilder* tableBuilder;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
    [self configureParser];
    [self updateData];
}

-(void)configureParser{
    XMLParser* xmlParser = [[XMLParser alloc] init];
    
    _parseController = [[ParseController alloc] init];
    _parseController.parser = xmlParser;
    
    __weak typeof(self) weakSelf = self;
    _parseController.completion = ^(NSMutableArray<NSDictionary*> * titles){
        weakSelf.tableBuilder.titles = titles;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.mainTableView reloadData];
        });
    };
}

-(void)updateData{
    [_parseController getData];
}

-(void)configureView{
    self.tableBuilder = [TableBuilder new];
    
    _mainTableView = [UITableView new];
    _mainTableView.delegate = self.tableBuilder;
    _mainTableView.dataSource = self.tableBuilder;
    _mainTableView.rowHeight = UITableViewAutomaticDimension;
    
    [self.view addSubview:_mainTableView];
    [self.view setNeedsLayout];
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    CGRect selfFrame = self.view.frame;
    _mainTableView.frame = CGRectMake(0, 24, selfFrame.size.width, selfFrame.size.height);
}
@end
