//
//  Parser.h
//  TedRssFeed
//
//  Created by Максуд on 09.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ParserDelegate <NSObject>

-(void)didEndParsing:(NSMutableArray<NSDictionary*>*) result;
-(void)parseError:(NSError*)error;

@end

@interface Parser : NSObject

@property(nonatomic, strong) NSData* data;
@property(nonatomic, weak) id<ParserDelegate> parseDelegate;

-(void)startParsing;
-(instancetype)initWithData:(NSData*)data;

@end
