//
//  RssTableViewCell.m
//  TedRssFeed
//
//  Created by Максуд on 08.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import "RssTableViewCell.h"
#import "Constants.h"

@interface RssTableViewCell()

@property (nonatomic, strong) UILabel* titleLabel;
@property (nonatomic, strong) UILabel* contentLabel;

@end

@implementation RssTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self != nil){
        self.titleLabel = [UILabel new];
        self.titleLabel.font = [UIFont systemFontOfSize:14];
        self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.titleLabel.numberOfLines = 0;
        
        self.contentLabel = [UILabel new];
        self.contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.contentLabel.font = [UIFont systemFontOfSize:14];
        self.contentLabel.numberOfLines = 0;
        
        self.contentLabel.hidden = YES;
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.contentLabel];
        self.clipsToBounds = YES;
    }
    
    return self;
}

-(void)setIsActive:(BOOL)isActive{
    self.contentLabel.hidden = !isActive;
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

-(void)setContent:(NSDictionary *)content{
    if([self.content isEqualToDictionary:content]){
        return;
    }
    _titleLabel.text = content[TitleKey];
    self.contentLabel.text = content[DescriptionKey];
    _content = content;
    [self setNeedsLayout];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    CGFloat padding = 8;
    
    [self.titleLabel sizeToFit];
    self.titleLabel.frame = CGRectMake(padding, padding, self.frame.size.width - padding, self.titleLabel.frame.size.height);
    
    [self.contentLabel sizeToFit];
    self.contentLabel.frame = CGRectMake(padding, self.titleLabel.frame.size.height + 4, self.frame.size.width - padding*2, self.contentLabel.frame.size.height);
}

@end
