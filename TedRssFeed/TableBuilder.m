//
//  TableBuilder.m
//  TedRssFeed
//
//  Created by Максуд on 09.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import "TableBuilder.h"
#import "RssTableViewCell.h"
#import "Constants.h"

@implementation TableBuilder{
    NSIndexPath *activeCellIndexPath;
}


//MARK: TableViewDelegate & UITableViewDatasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.titles.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RssTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(cell == nil){
        cell = [[RssTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.content = self.titles[indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self configureActiveCellOfTable:tableView atIndexPath:indexPath];
    [tableView beginUpdates];
    [tableView endUpdates];
}

-(void)configureActiveCellOfTable:(UITableView*)tableView atIndexPath:(NSIndexPath*)indexPath{
    if(activeCellIndexPath != nil && [activeCellIndexPath compare:indexPath] == NSOrderedSame){
        RssTableViewCell *cell = [tableView cellForRowAtIndexPath:activeCellIndexPath];
        cell.isActive = NO;
        activeCellIndexPath = nil;
    }else{
        if(activeCellIndexPath == nil){
            RssTableViewCell *activeCell = [tableView cellForRowAtIndexPath:indexPath];
            activeCell.isActive = YES;
        }else if(activeCellIndexPath != nil){
            RssTableViewCell *nonActiveCell = [tableView cellForRowAtIndexPath:activeCellIndexPath];
            RssTableViewCell *activeCell = [tableView cellForRowAtIndexPath:indexPath];
            nonActiveCell.isActive = NO;
            activeCell.isActive = YES;
        }
        activeCellIndexPath = indexPath;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary* titleDict = self.titles[indexPath.row];
    NSString *title = titleDict[TitleKey];
    NSString *desc = titleDict[DescriptionKey];
    if(activeCellIndexPath != nil && [activeCellIndexPath compare:indexPath] == NSOrderedSame){
        return [self heightForText:title] + [self heightForText:desc] + 8;
    }
    
    return [self heightForText:title];
}

-(CGFloat)heightForText:(NSString*)text{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    UIFont *font = [UIFont systemFontOfSize:14];
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    NSAttributedString *attributedText =
    [[NSAttributedString alloc]
     initWithString:text
     attributes:@
     {
     NSFontAttributeName: font
     }];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){width, CGFLOAT_MAX}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:context];
    CGFloat height = rect.size.height;
    
    return height;
}


@end
