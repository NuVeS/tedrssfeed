//
//  RssTableViewCell.h
//  TedRssFeed
//
//  Created by Максуд on 08.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RssTableViewCell : UITableViewCell

@property (nonatomic, strong) NSDictionary* content;
@property (nonatomic) BOOL isActive;

@end
