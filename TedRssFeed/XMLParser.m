//
//  XMLParser.m
//  TedRssFeed
//
//  Created by Максуд on 09.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import "XMLParser.h"
#import "Constants.h"

@interface XMLParser()<NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray *_objects;
@property (nonatomic, strong) NSXMLParser* parser;
@property (nonatomic, strong) NSMutableArray<NSDictionary*> *titles;

@end

@implementation XMLParser{
    NSOperationQueue *queue;
    
    NSMutableString *curDescription;
    NSString *curElement;
    NSMutableString *curTitle;
    NSMutableString *curDate;
}

-(instancetype)init{
    self = [super init];
    if(self != nil){
        self.titles = @[].mutableCopy;
    }
    return self;
}

-(instancetype)initWithData:(NSData *)data{
    self = [self init];
    if(self != nil){
        self.data = data;
    }
    
    return self;
}

-(void)setData:(NSData *)data{
    _parser = [[NSXMLParser alloc] initWithData:data];
    [_parser setDelegate:self];
}

-(void)startParsing{
    queue = [[NSOperationQueue alloc] init];
    [queue addOperationWithBlock:^{
        [_parser parse];
    }];
}

#pragma mark - Parsing

- (void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName
    attributes:(NSDictionary *)attributeDict  {
    
    curElement = elementName;
    if ([elementName isEqualToString:@"item"]) {
        curTitle = [NSMutableString string];
        curDescription = [NSMutableString string];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if ([curElement isEqualToString:TitleKey]) {
        [curTitle appendString:string];
    } else if ([curElement isEqualToString:DescriptionKey]) {
        [curDescription appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser
 didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName {
    if ([elementName isEqualToString:@"item"]) {
        NSMutableDictionary *newsItem = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                         curTitle, TitleKey,
                                         curDescription, DescriptionKey, nil];
        [self.titles addObject:newsItem];
        curTitle = nil;
        curDate = nil;
        curElement = nil;
        curDescription = nil;
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    [self.parseDelegate didEndParsing:self.titles];
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    [self.parseDelegate parseError:parseError];
}


@end
