//
//  Constants.h
//  TedRssFeed
//
//  Created by Максуд on 08.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import <Foundation/Foundation.h>
#ifndef Constants_h
#define Constants_h

extern NSString *const TitleKey;
extern NSString *const DescriptionKey;

#endif /* Constants_h */
