//
//  ViewController.h
//  TedRssFeed
//
//  Created by Максуд on 08.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParseController.h"

@interface ViewController : UIViewController

@property (nonatomic, strong) ParseController *parseController;

@end

