//
//  TableBuilder.h
//  TedRssFeed
//
//  Created by Максуд on 09.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableBuilder : NSObject<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray<NSDictionary*> *titles;

@end
