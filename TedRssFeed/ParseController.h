//
//  Controller.h
//  TedRssFeed
//
//  Created by Максуд on 09.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLParser.h"
#import "Parser.h"

typedef void  (^LoadCompletionBlock)(NSMutableArray<NSDictionary*>*);

@interface ParseController : NSObject

@property LoadCompletionBlock completion;
@property (nonatomic, strong) Parser *parser;

-(void)getData;


@end
