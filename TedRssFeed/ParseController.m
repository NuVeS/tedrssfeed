//
//  Controller.m
//  TedRssFeed
//
//  Created by Максуд on 09.02.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

#import "ParseController.h"

@interface ParseController()<ParserDelegate>

@end

@implementation ParseController

-(void)getData{
    NSString *dataUrl = @"https://www.ted.com/talks/rss";
    NSURL *url = [NSURL URLWithString:dataUrl];
    
    _parser.parseDelegate = self;
    NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession]
                                          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              _parser.data = data;
                                              [_parser startParsing];
                                          }];
    
    [downloadTask resume];
}

//Parser

-(void)didEndParsing:(NSMutableArray<NSDictionary *> *)result{
    _completion(result);
}

-(void)parseError:(NSError *)error{
    NSLog(@"%@", error);
}

@end
